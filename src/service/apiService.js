import { ToastAndroid } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Config from 'react-native-config';

import api from './network';
import { EXCHANGE_TOKEN_URL } from '../constants/constants';
import { createCommand } from '../utils/helpers';

export const getTasks = async () => {
  const requestBody = {
    sync_token: '*',
    resource_types: ['items'],
  };
  const response = await api.post('', requestBody);
  return response;
};

export const createTask = async (taskDetail) => {
  const command = createCommand('item_add', taskDetail);
  const response = await api.post('', { commands: [command] });
  return response;
};

export const completeTask = async (taskId) => {
  const command = createCommand('item_complete', { id: taskId });
  const response = await api.post('', { commands: [command] });
  return response;
};

export const updateTask = async (taskDetails) => {
  const command = createCommand('item_update', taskDetails);
  const response = await api.post('', { commands: [command] });
  return response;
};

export const getUpdatedTaskList = async (sync_token) => {
  const requestBody = {
    sync_token,
    resource_types: ['items'],
  };
  const response = await api.post('', requestBody);
  return response;
};

export const deleteTask = async (taskId) => {
  const command = createCommand('item_delete', { id: taskId });
  const response = await api.post('', { commands: [command] });
  return response;
};

export const getToken = async (code) => {
  const url = `${EXCHANGE_TOKEN_URL}?client_id=${Config.CLIENT_ID}&client_secret=${Config.CLIENT_SECRET}&code=${code}`;
  const response = await api.post(url);
  return response;
};

export const storeAccessToken = async (accessToken) => {
  try {
    await AsyncStorage.setItem('accessToken', accessToken);
    return accessToken;
  } catch (error) {
    console.log('Error: L: 85 apiService 👉🏻 ', error);
    ToastAndroid.show('Error: Something went wrong try after some time!', ToastAndroid.LONG);
  }
};

export const getAccessToken = async () => {
  try {
    const accessToken = await AsyncStorage.getItem('accessToken');
    if (accessToken) {
      return accessToken;
    }
    return null;
  } catch (error) {
    console.log('Error: L: 98 apiService 👉🏻 ', error);
    ToastAndroid.show('Error: Something went wrong!', ToastAndroid.LONG);
  }
};
