import React, { memo } from 'react';
import { Text, TouchableOpacity } from 'react-native';

import LocationIcon from 'react-native-vector-icons/EvilIcons';

import styles from './styles';

const Location = memo(() => {
  return (
    <TouchableOpacity style={styles.touchable}>
      <LocationIcon name="location" size={20} />
      <Text>Location</Text>
    </TouchableOpacity>
  );
});

export default Location;
