import React, { memo } from 'react';
import { Text, TouchableOpacity } from 'react-native';

import EditIcon from 'react-native-vector-icons/AntDesign';

import styles from './styles';

const EditTaskAction = memo(() => {
  return (
    <TouchableOpacity style={styles.touchable}>
      <EditIcon name="edit" size={20} />
      <Text>Edit task actions</Text>
    </TouchableOpacity>
  );
});

export default EditTaskAction;
