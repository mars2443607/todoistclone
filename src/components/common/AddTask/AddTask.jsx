import React, { useEffect, useState } from 'react';
import { ScrollView, TextInput, View, Keyboard, Pressable } from 'react-native';

import { FAB, Divider } from '@rneui/base';

import Calender from './Calender';
import Priority from './Priority';
import Reminders from './Reminders';
import Label from './Label';
import Inbox from './Inbox';
import Location from './Location';
import EditTaskAction from './EditTaskAction';

import { colors } from '../../../constants/constants';

import styles from './styles';

const AddTask = ({ addTaskDetails }) => {
  const [height, setHeight] = useState(0);
  const [isAddingTask, setIsAddingTask] = useState(false);
  const [taskDetails, setTaskDetails] = useState({ content: '', description: '' });

  const trimmedContent = taskDetails.content.trim();
  const trimmedDescription = taskDetails.description.trim();

  const openAddTaskModal = () => {
    setIsAddingTask(!isAddingTask);
  };
  const closeAddTaskModal = () => {
    Keyboard.dismiss();
    setIsAddingTask(false);
  };

  useEffect(() => {
    const keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', closeAddTaskModal);
    return () => {
      keyboardDidHideListener.remove();
    };
  }, []);

  const handleInputChange = (text, key) => {
    setTaskDetails((prev) => ({ ...prev, [key]: text }));
  };

  const addTask = async () => {
    closeAddTaskModal();
    setTaskDetails({ content: '', description: '' });
    await addTaskDetails({ content: trimmedContent, description: trimmedDescription });
  };

  useEffect(() => {
    const onShow = Keyboard.addListener('keyboardDidShow', keyboardWillShow);
    const onHide = Keyboard.addListener('keyboardDidHide', keyboardWillHide);

    return () => {
      onShow.remove();
      onHide.remove();
    };
  }, []);

  const keyboardWillShow = ({ endCoordinates: { height } }) => {
    setHeight(height - 100);
  };

  const keyboardWillHide = (event) => {
    setHeight(0);
  };
  return (
    <>
      <FAB
        placement="right"
        size="large"
        icon={styles.fabIcon}
        color={colors.red}
        onPress={openAddTaskModal}
        style={styles.fabButton}
      />
      {isAddingTask ? (
        <Pressable onPress={closeAddTaskModal} style={[styles.containerWrapper(height)]}>
          <View style={styles.container}>
            <TextInput
              autoFocus={true}
              placeholder="Task name"
              style={styles.taskTitle}
              multiline
              value={taskDetails.content}
              onChangeText={(text) => handleInputChange(text, 'content')}
            />
            <TextInput
              placeholder="Description"
              style={styles.taskDescription}
              multiline
              value={taskDetails.description}
              onChangeText={(text) => handleInputChange(text, 'description')}
            />
            <ScrollView horizontal keyboardShouldPersistTaps="handled" showsHorizontalScrollIndicator={false}>
              <Calender />
              <Priority />
              <Reminders />
              <Label />
              <Location />
              <EditTaskAction />
            </ScrollView>
            <Divider width={1} style={styles.divider} />
            <Inbox addTask={addTask} isDisabled={!taskDetails.content.trim()} />
          </View>
        </Pressable>
      ) : null}
    </>
  );
};

export default AddTask;
