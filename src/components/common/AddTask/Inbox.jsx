import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';

import InboxIcon from 'react-native-vector-icons/AntDesign';
import SendIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import CaretIcon from 'react-native-vector-icons/FontAwesome';

import { colors } from '../../../constants/constants';

import styles from './styles';

const Inbox = ({ addTask, isDisabled }) => {
  return (
    <View style={styles.inBoxWrapper}>
      <TouchableOpacity style={[styles.touchable, styles.touchableInbox]}>
        <InboxIcon name="inbox" size={18} color={colors.red} />
        <Text>Inbox</Text>
        <CaretIcon name="caret-down" size={10} />
      </TouchableOpacity>
      <TouchableOpacity
        style={[styles.button, styles.sendButton, isDisabled && styles.disabledAddTask]}
        onPress={addTask}
        disabled={isDisabled}
      >
        <SendIcon name="send" size={20} color={colors.primary} />
      </TouchableOpacity>
    </View>
  );
};

export default Inbox;
