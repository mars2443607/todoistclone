import React, { memo } from 'react';
import { Text, TouchableOpacity } from 'react-native';

import ClockIcon from 'react-native-vector-icons/MaterialCommunityIcons';

import styles from './styles';

const Calender = memo(() => {
  return (
    <TouchableOpacity style={styles.touchable}>
      <ClockIcon name="calendar-today" size={20} />
      <Text>Today</Text>
    </TouchableOpacity>
  );
});

export default Calender;
