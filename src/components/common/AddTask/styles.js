import { StyleSheet } from 'react-native';

import { colors, fontSize } from '../../../constants/constants';

const styles = StyleSheet.create({
  containerWrapper: (height) => ({
    position: 'absolute',
    zIndex: 99,
    justifyContent: 'flex-end',
    bottom: height,
    height: '100%',
  }),
  container: {
    backgroundColor: colors.primary,
    padding: 15,
  },
  inBoxWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: 10,
  },
  fabButton: {
    zIndex: 10,
  },
  fabIcon: { name: 'add', color: colors.primary },
  button: {
    backgroundColor: colors.red,
    justifyContent: 'center',
    alignItems: 'center',
  },
  sendButton: {
    width: 40,
    height: 40,
    borderRadius: 10,
  },
  taskTitle: {
    fontSize: fontSize.extraLarge,
    fontWeight: '600',
    padding: 0,
  },
  taskDescription: {
    fontSize: fontSize.medium,
    padding: 0,
    marginBottom: 10,
  },
  touchable: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    gap: 6,
    padding: 6,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: colors.grey,
    marginRight: 8,
  },
  touchableInbox: {
    width: '20%',
    borderWidth: 0,
  },
  disabledAddTask: {
    opacity: 0.5,
    backgroundColor: colors.disabled,
  },
  divider: {
    marginTop: 20,
  },
});

export default styles;
