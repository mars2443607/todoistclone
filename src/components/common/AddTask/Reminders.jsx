import React, { memo } from 'react';
import { Text, TouchableOpacity } from 'react-native';

import ReminderIcon from 'react-native-vector-icons/AntDesign';

import styles from './styles';

const Reminders = memo(() => {
  return (
    <TouchableOpacity style={styles.touchable}>
      <ReminderIcon name="clockcircleo" size={20} />
      <Text>Reminders</Text>
    </TouchableOpacity>
  );
});

export default Reminders;
