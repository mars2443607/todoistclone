import React, { memo } from 'react';
import { Text, TouchableOpacity } from 'react-native';

import TagIcon from 'react-native-vector-icons/SimpleLineIcons';

import styles from './styles';

const Label = memo(() => {
  return (
    <TouchableOpacity style={styles.touchable}>
      <TagIcon name="tag" size={20} />
      <Text>Labels</Text>
    </TouchableOpacity>
  );
});

export default Label;
