import React, { memo } from 'react';
import { Text, TouchableOpacity } from 'react-native';

import PriorityIcon from 'react-native-vector-icons/Feather';

import styles from './styles';

const Priority = memo(() => {
  return (
    <TouchableOpacity style={styles.touchable}>
      <PriorityIcon name="flag" size={20} />
      <Text>Priority</Text>
    </TouchableOpacity>
  );
});

export default Priority;
