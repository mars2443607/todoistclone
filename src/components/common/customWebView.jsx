import React, { useState } from 'react';
import { Modal, SafeAreaView, StyleSheet, Text, View } from 'react-native';
import { WebView } from 'react-native-webview';
import LottieView from 'lottie-react-native';

import { colors } from '../../constants/constants';

const CustomWebView = ({
  webViewVisible = false,
  onClose = () => {},
  _onNavigationStateChange = () => {},
  sourceURL = '',
}) => {
  const [isLoading, setIsLoading] = useState(true);

  const stopLoading = () => {
    setIsLoading(false);
  };

  return (
    <Modal animationType="animated" visible={webViewVisible} onRequestClose={onClose}>
      <SafeAreaView style={styles.container}>
        <WebView
          style={styles.webView}
          source={{ uri: sourceURL }}
          onNavigationStateChange={_onNavigationStateChange}
          onLoadStart={stopLoading}
        />
        {isLoading && (
          <View style={styles.loadingIndicator}>
            <LottieView style={styles.loading} source={require('../../assets/json/loading.json')} autoPlay loop />
            <Text style={styles.loadingText}>Please wait a moment...</Text>
          </View>
        )}
      </SafeAreaView>
    </Modal>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  webView: {
    flex: 1,
  },
  loadingIndicator: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'center',
    alignItems: 'center',
  },
  loading: {
    width: 400,
    height: 400,
  },
  loadingText: {
    fontSize: 20,
    fontWeight: 'bold',
    textShadowColor: colors.black,
    textShadowOffset: { width: 1, height: 1 },
    textShadowRadius: 2,
  },
});

export default CustomWebView;
