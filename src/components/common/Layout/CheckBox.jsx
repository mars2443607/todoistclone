import React, { useState } from 'react';
import { View, Text, TouchableOpacity, Alert } from 'react-native';

import styles from './styles';

const CheckBox = ({ onChecked }) => {
  const [checked, setChecked] = useState(false);

  const onCheckboxChecked = () => {
    if (!checked) {
      setChecked((prev) => !prev);
      Alert.alert('Complete Task ?', `This will mark task as completed.`, [
        {
          text: 'No',
          style: 'cancel',
          onPress: () => setChecked((prev) => !prev),
        },
        {
          text: 'Yes',
          style: 'cancel',
          onPress: () => onChecked(),
        },
      ]);
    }
  };

  return (
    <TouchableOpacity style={styles.checkbox} onPress={onCheckboxChecked}>
      <View style={[styles.checkboxContainer, checked && styles.checked]}>
        {checked && <Text style={styles.checkIcon}>✓</Text>}
      </View>
    </TouchableOpacity>
  );
};

export default CheckBox;
