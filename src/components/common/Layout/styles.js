import { StyleSheet } from 'react-native';

import { colors } from '../../../constants/constants';

const styles = StyleSheet.create({
  checkbox: {
    flexDirection: 'row',
    alignItems: 'center',
  },

  checkboxContainer: {
    width: 25,
    height: 25,
    borderWidth: 1,
    borderColor: colors.darkGrey,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 50,
    alignSelf: 'flex-start',
  },

  checked: {
    backgroundColor: colors.blue,
    borderColor: colors.blue,
  },

  checkIcon: {
    color: colors.primary,
  },

  centerItem: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default styles;
