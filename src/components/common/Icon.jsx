import React from 'react';

import MaterialIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import AntDesignIcon from 'react-native-vector-icons/AntDesign';

import { colors } from '../../constants/constants';

const Icon = ({ name, isActive }) => {
  const iconColor = isActive ? colors.red : colors.black;
  switch (name) {
    case 'Today':
      return <MaterialIcon name="calendar-today" size={22} color={iconColor} />;
    case 'Inbox':
      return <AntDesignIcon name="inbox" size={22} color={iconColor} />;
    case 'Search':
      return <AntDesignIcon name="search1" size={22} color={iconColor} />;
    case 'Browse':
      return <MaterialIcon name="menu" size={22} color={iconColor} />;
    default:
      return null;
  }
};

export default Icon;
