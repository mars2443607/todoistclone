import React, { useCallback, useMemo, useRef } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import BottomSheet, { BottomSheetBackdrop } from '@gorhom/bottom-sheet';

import MaterialCommunityIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import GraphIcon from 'react-native-vector-icons/SimpleLineIcons';

import { colors } from '../../constants/constants';

import styles from './styles';
import { getTodayDate } from '../../utils/helpers';
import { Divider } from '@rneui/base';

const SelectedTaskMoreActions = ({
  closeMoreActionsModal,
  addTaskDetails,
  task,
  openDeleteTaskAlert,
  closeTaskDetails,
  updateTaskDetails,
  completeTaskDetails,
}) => {
  const navigation = useNavigation();

  const bottomSheetRef = useRef(null);

  const snapPoints = useMemo(() => ['70%'], []);

  const renderBackdrop = useCallback(
    (props) => <BottomSheetBackdrop {...props} appearsOnIndex={0} disappearsOnIndex={-1} />,
    []
  );

  const addTaskToList = () => {
    addTaskDetails({ ...task });
    closeMoreActionsModal();
    closeTaskDetails();
  };

  const navigateToEditScreen = () => {
    const selectedTask = { content: task.content, description: task.description };
    navigation.navigate('Edit', { completeTask, selectedTask, updateTaskDetails: updateTask });
  };

  const completeTask = () => {
    completeTaskDetails(task.id);
    closeMoreActionsModal();
    closeTaskDetails();
  };

  const updateTask = (updatedTask) => {
    updateTaskDetails({ id: task.id, ...updatedTask });
  };

  return (
    <BottomSheet
      ref={bottomSheetRef}
      snapPoints={snapPoints}
      enablePanDownToClose
      onClose={closeMoreActionsModal}
      backdropComponent={renderBackdrop}
    >
      <View style={styles.moreActionsWrapper}>
        <Text style={styles.moreActionsShowDate}>Added on {getTodayDate(false)}</Text>
        <Divider width={1} />
        <View style={styles.moreActionsList}>
          <TouchableOpacity style={styles.moreActionsItemWrapper} onPress={navigateToEditScreen}>
            <MaterialCommunityIcon name="pencil-outline" size={20} />
            <Text>Edit task</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.moreActionsItemWrapper} onPress={addTaskToList}>
            <Ionicons name="duplicate-outline" size={20} />
            <Text>Duplicate task</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.moreActionsItemWrapper}>
            <Ionicons name="checkmark-done-circle-outline" size={20} />
            <Text>Show completed sub-tasks</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.moreActionsItemWrapper}>
            <GraphIcon name="graph" size={20} />
            <Text>Activity log</Text>
          </TouchableOpacity>
        </View>
        <Divider width={1} />
        <TouchableOpacity
          style={[styles.moreActionsItemWrapper, styles.moreActionsDeleteTaskWrapper]}
          onPress={openDeleteTaskAlert}
        >
          <MaterialCommunityIcon name="delete-outline" size={20} color={colors.red} />
          <Text style={styles.moreActionsDeleteTask}>Delete task</Text>
        </TouchableOpacity>
      </View>
    </BottomSheet>
  );
};

export default SelectedTaskMoreActions;
