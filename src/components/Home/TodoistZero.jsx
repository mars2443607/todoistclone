import React from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';

import styles from './styles';

const TodoistZero = () => {
  return (
    <>
      <View style={styles.todoistZeroWrapper}>
        <Image source={require('../../assets/images/todoist_zero_today.png')} />
        <View style={styles.todoistZeroBodyWrapper}>
          <Text style={styles.todoistZeroTitle}>Enjoy your day</Text>
          <Text style={styles.todoistZeroInfo} numberOfLines={2}>
            Today you completed tasks and reached #TodoistZero! share your awesomeness ↓
          </Text>
          <TouchableOpacity>
            <Text style={styles.todoistZeroFooter}>Share #TodoistZero</Text>
          </TouchableOpacity>
        </View>
      </View>
    </>
  );
};

export default TodoistZero;
