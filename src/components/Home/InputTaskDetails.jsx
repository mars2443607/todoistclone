import React from 'react';
import { Text, TouchableOpacity } from 'react-native';

import { useNavigation } from '@react-navigation/native';

import CheckBox from '../common/Layout/CheckBox';

import DescriptionIcon from 'react-native-vector-icons/Feather';

import styles from './styles';

const InputTaskDetails = ({ content, description, completeTask, updateTaskDetails }) => {
  const navigation = useNavigation();

  const navigateToEditScreen = () => {
    const selectedTask = { content, description };
    navigation.navigate('Edit', { completeTask, selectedTask, updateTaskDetails });
  };

  return (
    <>
      <TouchableOpacity style={[styles.alignItems, styles.taskTitle]} onPress={navigateToEditScreen}>
        <CheckBox onChecked={completeTask} />
        <Text numberOfLines={1}>{content}</Text>
      </TouchableOpacity>
      {description ? (
        <TouchableOpacity style={[styles.alignItems, styles.taskDescription]} onPress={navigateToEditScreen}>
          <DescriptionIcon name="align-left" size={25} />
          <Text numberOfLines={1}>{description}</Text>
        </TouchableOpacity>
      ) : null}
    </>
  );
};

export default InputTaskDetails;
