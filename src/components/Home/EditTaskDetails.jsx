import React, { useMemo, useRef, useEffect, useCallback } from 'react';
import { Text, View, TouchableOpacity, BackHandler } from 'react-native';

import { Divider } from '@rneui/base';

import BottomSheet, { BottomSheetBackdrop } from '@gorhom/bottom-sheet';

import InboxIcon from 'react-native-vector-icons/AntDesign';
import ThreeDotIcon from 'react-native-vector-icons/Entypo';
import InputTaskDetails from './InputTaskDetails';

import { colors } from '../../constants/constants';

import styles from './styles';

const EditTaskDetails = ({
  task,
  completeTaskDetails,
  updateTaskDetails,
  closeTaskDetails,
  openMoreActionsModal,
  closeMoreActionsModal,
}) => {
  const bottomSheetRef = useRef(null);

  const snapPoints = useMemo(() => ['80%', '100%'], []);

  useEffect(() => {
    const backAction = () => {
      bottomSheetRef.current?.close();
      return true;
    };
    const backHandler = BackHandler.addEventListener('hardwareBackPress', backAction);
    return () => backHandler.remove();
  }, []);

  const completeTask = () => {
    completeTaskDetails(task.id);
    closeMoreActionsModal();
    closeTaskDetails();
  };

  const updateTask = (updatedTask) => {
    updateTaskDetails({ id: task.id, ...updatedTask });
  };

  const renderBackdrop = useCallback(
    (props) => <BottomSheetBackdrop {...props} appearsOnIndex={0} disappearsOnIndex={-1} />,
    []
  );

  const BottomSheetBackground = ({ style }) => {
    return <View style={[{ borderRadius: 0, backgroundColor: colors.white }, { ...style }]} />;
  };

  return (
    <BottomSheet
      ref={bottomSheetRef}
      snapPoints={snapPoints}
      enablePanDownToClose
      onClose={closeTaskDetails}
      backdropComponent={renderBackdrop}
      backgroundComponent={(props) => <BottomSheetBackground {...props} />}
    >
      <View style={styles.container}>
        <View style={styles.moreInfoSection}>
          <TouchableOpacity style={[styles.alignItems, styles.addToInboxButton]}>
            <InboxIcon name="inbox" size={25} color={colors.red} />
            <Text>Inbox {'>'}</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.button} onPress={openMoreActionsModal}>
            <ThreeDotIcon name="dots-three-vertical" size={15} />
          </TouchableOpacity>
        </View>
        <InputTaskDetails
          content={task?.content}
          description={task?.description}
          completeTask={completeTask}
          updateTaskDetails={updateTask}
        />
        <TouchableOpacity style={styles.taskDate}></TouchableOpacity>
        <TouchableOpacity style={styles.taskPriority}></TouchableOpacity>
        <TouchableOpacity style={styles.taskLabel}></TouchableOpacity>
        <Divider width={1} style={styles.divider} />
      </View>
    </BottomSheet>
  );
};

export default EditTaskDetails;
