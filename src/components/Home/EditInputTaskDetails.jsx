import React, { useMemo, useState } from 'react';
import { Modal, Text, TextInput, TouchableOpacity, View } from 'react-native';

import CheckBox from '../common/Layout/CheckBox';

import DescriptionIcon from 'react-native-vector-icons/Feather';
import BackIcon from 'react-native-vector-icons/MaterialIcons';

import { colors } from '../../constants/constants';

import styles from './styles';

const EditInputTask = ({ selectedTask, onChecked, updateTaskDetails, onClose, isOpen }) => {
  const [taskDetails, setTaskDetails] = useState({
    content: selectedTask.content,
    description: selectedTask.description || '',
  });

  const trimmedContent = taskDetails.content.trim();
  const trimmedDescription = taskDetails.description.trim();

  const { content, description } = taskDetails;

  const handleInputChange = (text, key) => {
    setTaskDetails((prev) => ({ ...prev, [key]: text }));
  };

  const saveTaskDetails = () => {
    updateTaskDetails({ content: trimmedContent, description: trimmedDescription });
    onClose();
  };

  const isButtonDisabled = useMemo(() => {
    const isContentEmpty = content === '';
    const isContentAndDescriptionUnchanged =
      content === selectedTask.content && description === selectedTask.description;
    return isContentAndDescriptionUnchanged || isContentEmpty;
  }, [content, description, selectedTask]);

  return (
    <Modal onRequestClose={onClose} animationType="slide" visible={isOpen}>
      <View style={styles.editInputTaskWrapper}>
        <View style={styles.editInputTaskHeader}>
          <TouchableOpacity onPress={onClose} style={styles.editInputTaskIcon}>
            <BackIcon name="keyboard-backspace" color={colors.black} size={25} />
            <Text style={styles.editInputTaskTitle}>Edit task</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={saveTaskDetails} disabled={isButtonDisabled}>
            <Text style={[styles.editInputTaskSaveButton, isButtonDisabled && styles.editInputTaskSaveButtonDisabled]}>
              Save
            </Text>
          </TouchableOpacity>
        </View>
        <View style={styles.alignItems}>
          <CheckBox onChecked={onChecked} />
          <TextInput
            multiline
            placeholder="Type your task"
            value={content}
            style={styles.marginPaddingZero}
            onChangeText={(text) => handleInputChange(text, 'content')}
          />
        </View>
        <View style={styles.alignItems}>
          <DescriptionIcon name="align-left" size={25} />
          <TextInput
            multiline
            placeholder="Description"
            value={description}
            style={styles.marginPaddingZero}
            onChangeText={(text) => handleInputChange(text, 'description')}
          />
        </View>
      </View>
    </Modal>
  );
};

export default EditInputTask;
