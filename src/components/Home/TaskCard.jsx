import React from 'react';
import { Text, View, TouchableOpacity } from 'react-native';

import CheckBox from '../common/Layout/CheckBox';

import styles from './styles';

const TaskCard = ({ task, openTaskDetails, completeTaskDetails, isLoading }) => {
  const completeTask = () => {
    completeTaskDetails(task.id);
  };

  return (
    <>
      <TouchableOpacity style={styles.taskCard} onPress={!isLoading ? () => openTaskDetails(task) : null}>
        <CheckBox onChecked={completeTask} />
        <View style={styles.taskDetails}>
          <Text numberOfLines={1} style={styles.taskTitle}>
            {task.content}
          </Text>
          {task.description ? (
            <Text numberOfLines={1} style={styles.taskDescription}>
              {task.description}
            </Text>
          ) : null}
        </View>
      </TouchableOpacity>
    </>
  );
};

export default TaskCard;
