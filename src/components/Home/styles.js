import { StyleSheet } from 'react-native';

import { colors, fontSize } from '../../constants/constants';

const styles = StyleSheet.create({
  // Edit Task Details

  container: {
    flex: 1,
    padding: 20,
    backgroundColor: colors.primary,
  },
  moreInfoSection: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  alignItems: {
    flexDirection: 'row',
    gap: 20,
    padding: 10,
    width: '90%',
  },
  subTaskSection: { paddingTop: 5 },

  divider: {
    marginTop: 10,
  },

  // EditInputTask component styles

  editInputTaskWrapper: {
    flex: 1,
    padding: 20,
    height: '100%',
    width: '100%',
  },

  editInputTaskHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 20,
    alignItems: 'center',
  },

  editInputTaskTitle: {
    fontWeight: 'bold',
    fontSize: fontSize.extraLarge,
    color: colors.black,
  },

  editInputTaskSaveButton: {
    fontSize: fontSize.large,
    fontWeight: 'bold',
    color: colors.black,
  },

  editInputTaskSaveButtonDisabled: {
    color: colors.buttonDisabled,
  },

  marginPaddingZero: {
    margin: 0,
    padding: 0,
  },

  editInputTaskIcon: {
    flexDirection: 'row',
    alignItems: 'center',
    gap: 20,
  },

  // Task Card

  taskCard: {
    flexDirection: 'row',
    padding: 15,
    marginLeft: 10,
    borderBottomWidth: 1,
    borderBottomColor: colors.black,
    gap: 10,
  },

  marginPaddingZero: {
    margin: 0,
    padding: 0,
  },

  taskDetails: {
    gap: 4,
    alignSelf: 'flex-start',
    flex: 1,
  },

  taskTitle: {
    width: '80%',
    fontSize: fontSize.large,
    fontWeight: '600',
  },

  taskDescription: {
    width: '80%',
    fontSize: fontSize.medium,
  },

  // TODOIST ZERO

  todoistZeroWrapper: {
    height: '100%',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  todoistZeroBodyWrapper: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
  },
  todoistZeroTitle: {
    fontSize: fontSize.large,
    fontWeight: '600',
    color: colors.charcoalGray,
    marginVertical: 8,
  },
  todoistZeroInfo: {
    fontSize: fontSize.medium,
    color: colors.mediumDarkGrey,
    lineHeight: 22,
    textAlign: 'center',
    paddingHorizontal: 24,
  },
  todoistZeroFooter: {
    fontSize: fontSize.medium,
    fontWeight: '600',
    color: colors.red,
    marginTop: 12,
    paddingHorizontal: 12,
  },

  // SelectedTAskMoreActions styles

  moreActionsWrapper: {
    flex: 1,
    padding: 20,
  },
  moreActionsShowDate: {
    paddingBottom: 20,
    color: colors.grey,
    fontSize: fontSize.small,
  },
  moreActionsList: {
    paddingVertical: 20,
    gap: 30,
  },
  moreActionsItemWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    gap: 20,
  },
  moreActionsDeleteTaskWrapper: {
    paddingVertical: 20,
  },
  moreActionsDeleteTask: {
    color: colors.red,
  },
});

export default styles;
