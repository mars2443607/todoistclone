import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import HomeStack from '../screens/HomeStack/HomeStack';
import Inbox from '../screens/Inbox/Inbox';
import Search from '../screens/Search/Search';
import Browse from '../screens/Browse/Browse';

import { colors } from '../constants/constants';
import Icon from '../components/common/Icon';

const Tab = createBottomTabNavigator();

const BottomTabNavigator = () => (
  <Tab.Navigator
    tabBar={(props) => <Tabs {...props} />}
    screenOptions={{ tabBarHideOnKeyboard: true, headerShown: false }}
  >
    <Tab.Screen name="Today" component={HomeStack} />
    <Tab.Screen name="Inbox" component={Inbox} />
    <Tab.Screen name="Search" component={Search} />
    <Tab.Screen name="Browse" component={Browse} />
  </Tab.Navigator>
);

const Tabs = ({ state, descriptors, navigation }) => {
  return (
    <View style={styles.tabNavigator}>
      {state.routes.map((route, index) => {
        const { options } = descriptors[route.key];
        const label = options.tabBarLabel ? options.tabBarLabel : options.title ? options.title : route.name;
        const isFocused = state.index === index;
        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
          });
          if (!isFocused && !event.defaultPrevented) {
            navigation.navigate(route.name);
          }
        };
        const onLongPress = () => {
          navigation.emit({
            type: 'tabLongPress',
            target: route.key,
          });
        };
        return (
          <TouchableOpacity
            key={route.key}
            underlayColor={colors.primary}
            accessibilityRole="button"
            accessibilityStates={isFocused ? ['selected'] : []}
            accessibilityLabel={options.tabBarAccessibilityLabel}
            testID={options.tabBarTestID}
            onPress={onPress}
            onLongPress={onLongPress}
            style={styles.button}
          >
            <Icon name={label} isActive={isFocused} />
            <Text style={{ color: isFocused ? colors.red : colors.black }}>{label}</Text>
          </TouchableOpacity>
        );
      })}
    </View>
  );
};

const styles = StyleSheet.create({
  tabNavigator: {
    flexDirection: 'row',
    backgroundColor: colors.charcoal,
    height: 100,
    justifyContent: 'center',
    alignItems: 'center',
  },
  button: {
    flex: 1,
    alignItems: 'center',
    gap: 5,
  },
});

export default BottomTabNavigator;
