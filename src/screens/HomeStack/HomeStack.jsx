import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import HomeScreen from './Home/HomeScreen';
import EditTaskScreen from './Edit/EditTaskScreen';

const Stack = createStackNavigator();

const HomeStack = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Home" component={HomeScreen} />
      <Stack.Screen name="Edit" component={EditTaskScreen} />
    </Stack.Navigator>
  );
};

export default HomeStack;
