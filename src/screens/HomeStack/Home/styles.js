import { StyleSheet } from 'react-native';

import { colors, fontSize } from '../../../constants/constants';

export default StyleSheet.create({
  // Todoist styles

  centerItemList: {
    position: 'relative',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },

  centerLoader: {
    position: 'absolute',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },

  taskListContainer: (isLoading) => ({
    width: '100%',
    height: '100%',
    opacity: isLoading ? 0.5 : 1,
  }),

  todayData: {
    padding: 16,
    borderBottomWidth: 1,
    borderBottomColor: colors.black,
    fontWeight: 'bold',
    fontSize: fontSize.large,
    backgroundColor: colors.primary,
    color: colors.black,
    width: '100%',
  },
});
