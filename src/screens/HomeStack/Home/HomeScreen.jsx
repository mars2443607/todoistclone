import React, { useEffect, useState } from 'react';
import { View, ActivityIndicator, ToastAndroid, Text, FlatList, Alert } from 'react-native';

import Loader from '../../../components/common/Layout/Loader';
import TodoistZero from '../../../components/Home/TodoistZero';
import AddTask from '../../../components/common/AddTask/AddTask';
import TaskCard from '../../../components/Home/TaskCard';
import EditTaskDetails from '../../../components/Home/EditTaskDetails';
import SelectedTaskMoreActions from '../../../components/Home/SelectedTaskMoreActions';

import {
  completeTask,
  createTask,
  deleteTask,
  getTasks,
  getUpdatedTaskList,
  updateTask,
} from '../../../service/apiService';
import { getFormattedDate, getTodayDate } from '../../../utils/helpers';

import styles from './styles';
import { useNavigation } from '@react-navigation/native';

const HomeScreen = () => {
  const [tasks, setTasks] = useState({
    list: [],
    isLoading: true,
    syncToken: '',
  });
  const [selectedTask, setSelectedTask] = useState({
    details: null,
    isOpen: false,
    isMoreActionsModalOpen: false,
  });

  const navigation = useNavigation();

  const handleSyncResponse = async (response) => {
    const {
      data: { sync_status, sync_token },
    } = response;
    if (sync_status) {
      const operationUUID = Object.keys(sync_status)[0];
      if (sync_status[operationUUID] === 'ok') {
        const {
          data: { items },
        } = await getUpdatedTaskList(tasks.syncToken);
        setTasks((prev) => ({ ...prev, syncToken: sync_token }));
        return items;
      }
    }
    throw new Error('Something went wrong! Please try after some time!');
  };

  const openTaskDetails = (task) => {
    if (selectedTask.details) {
      closeTaskDetails();
      return;
    }
    setSelectedTask((prev) => ({ ...prev, details: task, isOpen: true }));
  };

  const closeTaskDetails = () => {
    setSelectedTask((prev) => ({ ...prev, details: null, isOpen: false }));
  };

  const addTaskDetails = async (taskDetails) => {
    try {
      setTasks((prev) => ({ ...prev, isLoading: true }));
      const response = await createTask(taskDetails);
      if (response.status === 200) {
        const updatedItems = await handleSyncResponse(response);
        if (updatedItems) {
          setTasks((prev) => ({ ...prev, list: [...prev.list, updatedItems[0]], isLoading: false }));
          ToastAndroid.show('Task added!', ToastAndroid.LONG);
        }
      }
    } catch (error) {
      setTasks((prev) => ({ ...prev, isLoading: false }));
      console.log('Error: addTaskDetails() 👉🏻', error);
      ToastAndroid.show(error.message, ToastAndroid.LONG);
    }
  };

  const updateTaskDetails = async (taskDetails) => {
    try {
      setTasks((prev) => ({ ...prev, isLoading: true }));
      const response = await updateTask(taskDetails);
      if (response.status === 200) {
        const updatedItems = await handleSyncResponse(response);
        if (updatedItems) {
          setTasks((prev) => ({
            ...prev,
            list: prev.list.map((task) => (task.id === updatedItems[0].id ? updatedItems[0] : task)),
            isLoading: false,
          }));
          setSelectedTask((prev) => ({ ...prev, details: updatedItems[0] }));
          ToastAndroid.show('Task Updated!', ToastAndroid.LONG);
        }
      }
    } catch (error) {
      setTasks((prev) => ({ ...prev, isLoading: false }));
      console.log('Error: updateTaskDetails() 👉🏻', error);
      ToastAndroid.show(error.message, ToastAndroid.LONG);
    }
  };

  const completeTaskDetails = async (taskId) => {
    try {
      setTasks((prev) => ({ ...prev, isLoading: true }));
      const response = await completeTask(taskId);
      if (response.status === 200) {
        const updatedItems = await handleSyncResponse(response);
        if (updatedItems) {
          setTasks((prev) => ({
            ...prev,
            list: prev.list.filter((task) => task.id !== taskId),
            isLoading: false,
          }));
          ToastAndroid.show('Task Completed!', ToastAndroid.LONG);
        }
      }
    } catch (error) {
      setTasks((prev) => ({ ...prev, isLoading: false }));
      console.log('Error: completeTaskDetails() 👉🏻', error);
      ToastAndroid.show(error.message, ToastAndroid.LONG);
    }
  };

  const deleteTaskDetails = async (taskId) => {
    try {
      setTasks((prev) => ({ ...prev, isLoading: true }));
      const response = await deleteTask(taskId);
      if (response.status === 200) {
        const updatedItems = await handleSyncResponse(response);
        if (updatedItems) {
          setTasks((prev) => ({
            ...prev,
            list: prev.list.filter((task) => task.id !== taskId),
            isLoading: false,
          }));
          closeTaskDetails();
          ToastAndroid.show('Task Deleted!', ToastAndroid.LONG);
        }
      }
    } catch (error) {
      setTasks((prev) => ({ ...prev, isLoading: false }));
      console.log('Error: deleteTaskDetails() 👉🏻', error);
      ToastAndroid.show(error.message, ToastAndroid.LONG);
    }
  };

  const openMoreActionsModal = () => {
    setSelectedTask((prev) => ({ ...prev, isMoreActionsModalOpen: true }));
  };

  const closeMoreActionsModal = () => {
    setSelectedTask((prev) => ({ ...prev, isMoreActionsModalOpen: false }));
  };

  const openDeleteTaskAlert = () => {
    const {
      details: { id, content },
    } = selectedTask;
    Alert.alert('Delete Task ?', `This will permanently delete ${content || 'task'} and can not be undone.`, [
      {
        text: 'No',
        style: 'cancel',
      },
      {
        text: 'Yes',
        style: 'cancel',
        onPress: () => deleteTaskDetails(id),
      },
    ]);
    closeMoreActionsModal();
  };

  useEffect(() => {
    (async () => {
      try {
        setTasks((prev) => ({ ...prev, isLoading: true }));
        const {
          data: { items, sync_token },
        } = await getTasks();
        const currentDate = getFormattedDate();
        setTasks((prev) => ({
          ...prev,
          isLoading: false,
          list: [...items.filter((task) => task.added_at.split('T')[0] === currentDate)],
          syncToken: sync_token,
        }));
      } catch (error) {
        console.log('Error: L: 183 HomeScreen  👉🏻', error);
        setTasks((prev) => ({ ...prev, isLoading: false }));
        ToastAndroid.show(error.message, ToastAndroid.LONG);
      }
    })();
  }, []);

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      setSelectedTask((prev) => ({ ...prev, isMoreActionsModalOpen: false, isOpen: false }));
    });

    return unsubscribe;
  }, [navigation]);

  return tasks.isLoading && !tasks.list.length ? (
    <Loader />
  ) : (
    <View style={styles.centerItemList}>
      <AddTask addTaskDetails={addTaskDetails} />
      {tasks.list.length ? (
        <>
          {tasks.isLoading ? (
            <View style={styles.centerLoader}>
              <ActivityIndicator size="large" />
            </View>
          ) : null}
          <Text style={[styles.todayData]}>{getTodayDate()}</Text>
          <FlatList
            style={styles.taskListContainer(tasks.isLoading)}
            data={tasks.list}
            keyExtractor={(item) => item.id}
            renderItem={({ item }) => (
              <TaskCard
                task={item}
                isLoading={tasks.isLoading}
                completeTaskDetails={completeTaskDetails}
                openTaskDetails={openTaskDetails}
              />
            )}
          />
        </>
      ) : (
        <TodoistZero />
      )}
      {selectedTask.isOpen ? (
        <EditTaskDetails
          task={selectedTask.details}
          updateTaskDetails={updateTaskDetails}
          completeTaskDetails={completeTaskDetails}
          closeTaskDetails={closeTaskDetails}
          openMoreActionsModal={openMoreActionsModal}
          closeMoreActionsModal={closeMoreActionsModal}
        />
      ) : null}
      {selectedTask.isMoreActionsModalOpen ? (
        <SelectedTaskMoreActions
          task={selectedTask.details}
          closeMoreActionsModal={closeMoreActionsModal}
          openDeleteTaskAlert={openDeleteTaskAlert}
          updateTaskDetails={updateTaskDetails}
          completeTaskDetails={completeTaskDetails}
          addTaskDetails={addTaskDetails}
          closeTaskDetails={closeTaskDetails}
        />
      ) : null}
    </View>
  );
};

export default HomeScreen;
