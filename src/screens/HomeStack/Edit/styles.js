import { StyleSheet } from 'react-native';
import { colors, fontSize } from '../../../constants/constants';

const styles = StyleSheet.create({
  editInputTaskWrapper: {
    flex: 1,
    padding: 20,
    height: '100%',
    width: '100%',
  },

  editInputTaskSaveButton: {
    fontSize: fontSize.large,
    fontWeight: 'bold',
    color: colors.black,
    marginRight: 20,
  },

  editInputTaskSaveButtonDisabled: {
    color: colors.buttonDisabled,
  },

  marginPaddingZero: {
    margin: 0,
    padding: 0,
  },

  alignItems: {
    flexDirection: 'row',
    gap: 20,
    padding: 10,
    width: '90%',
  },
});

export default styles;
