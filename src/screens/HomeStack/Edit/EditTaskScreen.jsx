import React, { useCallback, useEffect, useLayoutEffect, useMemo, useState } from 'react';
import { View, Text, TouchableOpacity, TextInput } from 'react-native';

import DescriptionIcon from 'react-native-vector-icons/Feather';
import CheckBox from '../../../components/common/Layout/CheckBox';

import styles from './styles';

const EditTaskScreen = ({ route, navigation }) => {
  const { completeTask, selectedTask, updateTaskDetails } = route.params;

  const [taskDetails, setTaskDetails] = useState({
    content: selectedTask.content,
    description: selectedTask.description || '',
  });
  const [saving, setSaving] = useState(false);
  const { content, description } = taskDetails;

  const handleInputChange = (text, key) => {
    setTaskDetails((prev) => ({ ...prev, [key]: text }));
  };

  const saveTaskDetails = () => {
    updateTaskDetails({ content: content.trim(), description: description.trim() });
    navigation.goBack();
  };

  const isButtonDisabled = useMemo(() => {
    const isContentEmpty = content === '';
    const isContentAndDescriptionUnchanged =
      content === selectedTask.content && description === selectedTask.description;
    return isContentAndDescriptionUnchanged || isContentEmpty;
  }, [content, description, selectedTask]);

  const completeTaskDetails = () => {
    completeTask();
    navigation.goBack();
  };

  useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <TouchableOpacity onPress={() => setSaving(true)} disabled={isButtonDisabled}>
          <Text style={[styles.editInputTaskSaveButton, isButtonDisabled && styles.editInputTaskSaveButtonDisabled]}>
            Save
          </Text>
        </TouchableOpacity>
      ),
    });
  }, [navigation, isButtonDisabled]);

  useEffect(() => {
    if (saving) {
      saveTaskDetails();
      setSaving(false);
    }
  }, [saving]);

  return (
    <View style={styles.editInputTaskWrapper}>
      <View style={styles.alignItems}>
        <CheckBox onChecked={completeTaskDetails} />
        <TextInput
          multiline
          placeholder="Type your task"
          value={content}
          style={styles.marginPaddingZero}
          onChangeText={(text) => handleInputChange(text, 'content')}
        />
      </View>
      <View style={styles.alignItems}>
        <DescriptionIcon name="align-left" size={25} />
        <TextInput
          multiline
          placeholder="Description"
          value={description}
          style={styles.marginPaddingZero}
          onChangeText={(text) => handleInputChange(text, 'description')}
        />
      </View>
    </View>
  );
};

export default EditTaskScreen;
