import { StyleSheet } from 'react-native';

import { colors, fontSize } from '../../constants/constants';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.red,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 20,
  },
  imageWrapper: {
    alignItems: 'center',
    position: 'absolute',
    top: 0,
  },
  todoistTitle: {
    fontSize: fontSize.doubleLarge,
    fontWeight: 'bold',
    color: colors.primary,
    textAlign: 'center',
    marginTop: 20,
    textShadowOffset: { width: -4, height: 8 },
    textShadowRadius: 6,
    textShadowColor: colors.darkShade,
  },
  button: {
    marginTop: 50,
    padding: 20,
    backgroundColor: colors.primary,
    borderRadius: 50,
  },
  buttonText: {
    fontSize: fontSize.large,
    color: colors.black,
    fontWeight: 'bold',
    textAlign: 'center',
  },
});

export default styles;
