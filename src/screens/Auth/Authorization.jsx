import React, { useCallback, useState } from 'react';
import { View, Text, Image, Pressable } from 'react-native';
import Config from 'react-native-config';
import URLParse from 'url-parse';

import CustomWebView from '../../components/common/customWebView';

import { AUTHORIZATION_URL } from '../../constants/constants';
import { getUUID } from '../../utils/helpers';
import { getToken, storeAccessToken } from '../../service/apiService';

import styles from './styles';

const Authorization = ({ updateAccessToken }) => {
  const [webViewVisible, setWebViewVisible] = useState(false);
  const [state, setState] = useState(getUUID());

  const openWebView = useCallback(() => {
    setWebViewVisible(true);
  }, []);

  const closeWebView = useCallback(() => {
    setWebViewVisible(false);
  }, []);

  const _onNavigationStateChange = useCallback(
    async (webViewState) => {
      try {
        const parsedUrl = new URLParse(webViewState.url, true);
        if (parsedUrl.query.error) {
          setWebViewVisible(false);
        }
        if (parsedUrl.query.code && parsedUrl.query.state) {
          const code = parsedUrl.query.code;
          const responseState = parsedUrl.query.state;
          setWebViewVisible(false);
          if (state === responseState) {
            const response = await getToken(code);
            const { access_token } = response.data;
            const storedAccessToken = await storeAccessToken(access_token);
            updateAccessToken(storedAccessToken);
          }
        }
      } catch (error) {
        console.log('Error: _onNavigationStateChange() 👉🏻', error);
        ToastAndroid.show('Error: Storing access token!', ToastAndroid.LONG);
      }
    },
    [state, updateAccessToken]
  );

  return (
    <View style={styles.container}>
      <View style={styles.imageWrapper}>
        <Image source={require('../../assets/images/todoist_home.png')} />
      </View>
      <Image style={{ resizeMode: 'contain' }} source={require('../../assets/images/todoist_bg.png')} />
      <Text style={styles.todoistTitle}>Organize Your work and life, finally.</Text>
      <Pressable onPress={openWebView} style={styles.button}>
        <Text style={styles.buttonText}>Let's Get Started !</Text>
      </Pressable>
      <CustomWebView
        webViewVisible={webViewVisible}
        onClose={closeWebView}
        _onNavigationStateChange={_onNavigationStateChange}
        sourceURL={`${AUTHORIZATION_URL}?client_id=${Config.CLIENT_ID}&scope=data:read_write,data:delete&state=${state}`}
      />
    </View>
  );
};

export default Authorization;
