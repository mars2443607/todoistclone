import React from 'react';
import { Text, View } from 'react-native';

const Browse = () => {
  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <Text>Browse</Text>
    </View>
  );
};

export default Browse;
