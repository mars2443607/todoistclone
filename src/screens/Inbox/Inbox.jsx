import React, { useRef } from 'react';
import { View, StyleSheet, Animated } from 'react-native';

const Inbox = () => {
  const scrolling = useRef(new Animated.Value(0)).current;
  const translation = scrolling.interpolate({
    inputRange: [100, 150],
    outputRange: [-100, 0],
    extrapolate: 'clamp',
  });

  return (
    <>
      <Animated.View style={styles.headerView(translation)} />
      <Animated.ScrollView
        style={styles.scrollView}
        onScroll={Animated.event(
          [
            {
              nativeEvent: {
                contentOffset: {
                  y: scrolling,
                },
              },
            },
          ],
          {
            useNativeDriver: true,
          }
        )}
        scrollEventThrottle={16}
      >
        <View style={styles.emptyView} />
      </Animated.ScrollView>
    </>
  );
};

const styles = StyleSheet.create({
  headerView: (translation) => ({
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    height: 80,
    backgroundColor: 'tomato',
    transform: [{ translateY: translation }],
  }),
  scrollView: {
    flex: 1,
  },
  emptyView: {
    flex: 1,
    height: 1000,
  },
});
export default Inbox;
