export const getTodayDate = (showDay = true) => {
  const currentDate = new Date();
  const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  const daysOfWeek = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

  const day = currentDate.getDate();
  const month = months[currentDate.getMonth()];
  const dayOfWeek = daysOfWeek[currentDate.getDay()];

  return `${day} ${month} ${showDay ? `● ${dayOfWeek}` : ''}`;
};

export const getFormattedDate = () => {
  const currentDate = new Date();
  const formattedDate = `${currentDate.getFullYear()}-${(currentDate.getMonth() + 1)
    .toString()
    .padStart(2, '0')}-${currentDate.getDate().toString().padStart(2, '0')}`;
  return formattedDate;
};

export const getUUID = () => {
  return Date.now().toString();
};

export const createCommand = (type, args) => ({
  type,
  uuid: getUUID(),
  args,
  temp_id: '',
});
