# TodoistClone :

- This is a mobile app project that replicates the functionality of Todoist, a popular task management application. The app allows users to create, read, update, and delete tasks, as well as synchronize their tasks with the Todoist web platform.

## Features : 

- Authentication: Authenticate users to access and synchronize tasks with the Todoist web platform.
- Task Management: Create, read, update, and delete tasks.
- Task Duplication: Duplicate tasks for easy replication.
- Task Completion: Completion of task.
- Sync API Integration: Integrate the Todoist sync API to synchronize tasks between the mobile app and the web platform.


## Installation :

- To run the Todoist clone app locally on your machine, follow these steps:

## Prerequisites :

- minimum required Node.js Version >= 18
- Node.js and npm installed on your machine.
- React Native CLI installed globally (npm install -g react-native-cli).
- Android Studio or Xcode installed for running on simulators or physical devices.

## Clone the Repository :

```
git clone <repository-url>
cd todoist-clone-app
```

## Install Dependencies : 

```
npm install
```

## Run the Application : 

- for ios below command build the iOS app and start the simulator automatically.
```
react-native run-ios
```

- for android below command build the Android app and start the emulator automatically.
```
react-native run-android
```

