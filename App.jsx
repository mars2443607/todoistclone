import React, { useCallback, useEffect, useState } from 'react';
import { ActivityIndicator, StyleSheet, View, LogBox } from 'react-native';
import { GestureHandlerRootView } from 'react-native-gesture-handler';

import AppNavigator from './src/navigation/AppNavigator';
import Authorization from './src/screens/Auth/Authorization';

import { getAccessToken } from './src/service/apiService';

LogBox.ignoreLogs(['Non-serializable values were found in the navigation state']);

const App = () => {
  const [accessToken, setAccessToken] = useState(null);
  const [isLoading, setIsLoading] = useState(true);

  // function to update the access token
  const updateAccessToken = useCallback((token) => {
    setAccessToken(token);
  }, []);

  useEffect(() => {
    // function to fetch the access token from the API service. This function attempts to retrieve the access token required for authentication.
    const fetchAccessToken = async () => {
      try {
        const newAccessToken = await getAccessToken();
        setAccessToken(newAccessToken);
      } catch (error) {
        console.log('Error: fetchAccessToken() 👉🏻', error);
      } finally {
        setIsLoading(false);
      }
    };
    fetchAccessToken();
  }, []);

  if (isLoading) {
    return (
      <View style={styles.indicatorWrapper}>
        <ActivityIndicator size="large" />
      </View>
    );
  }

  if (accessToken) {
    return (
      <GestureHandlerRootView style={styles.container}>
        <AppNavigator />
      </GestureHandlerRootView>
    );
  }

  return <Authorization updateAccessToken={updateAccessToken} />;
};

const styles = StyleSheet.create({
  indicatorWrapper: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  container: {
    flex: 1,
  },
});

export default App;
